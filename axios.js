const form = document.querySelector('#searchTV');

form.addEventListener('submit', async (event) => {
    event.preventDefault();
    const userInput = form.elements.q.value;

    const res = await axios.get(`https://api.tvmaze.com/search/shows/?q=${userInput}`);
    console.log(res.data);

    const img = document.createElement('img');
    img.src = res.data[0].show.image.medium;

    document.body.append(img);

    const div = document.querySelector('.img');
    div.append(img);

    const name = document.createElement('p');
    name.textContent = res.data[0].show.name;
    name.setAttribute("id", "film")
    document.body.append(name);

    const div2 = document.querySelector('.film_name');
    div2.append(name);

    const genre = document.createElement('p');
    genre.textContent = res.data[0].show.genres;
    document.body.append(genre);

    div2.append(genre);
})